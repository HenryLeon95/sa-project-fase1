# <h1 align="center">PROYECTO DE SOFTWARE AVANZADO</h1>
<h3 >Desaroolado por: Henry Francisco León Hernández - 201503577 </h3>
<h3 >Año: 2022 </h3>

## FASE 1 - DOCUMENTACIÓN


## Análisis y solución propuesta
El sistema <strong>yoVoto</strong> realizará su primer funcionamiento en la facultad de Ingeniería de Ciencias y Sistemas de la Universidad de San Carlos de Guatemala, posteriormente en la Universidad y luego en todo el país. Permitiendo a cualquier persona interna o externa al pais pueda emitir su voto fatisfactoriamente.<br>
Por lo tanto, el presente proyecto contará con automatización en su desarrollo para evidenciar los cambios en tiempo real con total seguridad de que funcionará en cada actualización del sistema y así permitir al desarrollador trabajar de manera eficiente y enfocarse en el desarrollo de sus respectivas tareas.<br>
Para ello se estandarizarán buenas prácticas, metodologías de branching, metodologías ágiles, automatización en pruebas(unitarias, integración, funcionales y de estrés) y sobre todo se contará con CI/CD para poder garantizar un buen funcionamiento del ambiente de producción y de desarrollo.<br>
Con respecto a la metodología, se utilizará la metodología en espiral ya que el desarrollo continuo o repetido ayuda en la gestión de riesgos y un punto importante es que siempre hay espacio para atender los comentarios de los clientes.<br>
El modelo branching a utilizar será gitflow el cual ofrece entregas interactivas. También nos permitirá controlar las versiones y así trabajar cada rama por separada con la opción de tener la rama main o master en producción y la de development en el ambiente de desarrollo para pruebas internas y así probar las actualizaciones antes del pase a producción.

## Implementación
- Planificación: Se definirá entonces un cronograma, con los archivos y documentos reunidos por el equipo de trabajo. Además se desglosarán las actividades con base en el diseño del producto.
- Análisis: Se validará el prototipo pretendido, acorde a los plazos de tiempo y presupuesto entregado al cliente.
- Desarrollo: Se desarrolla y valida el prototipo (desarrollo de software, pruebas y despliegue), según el alcance y las funciones definidas en la etapa anterior.
- Evaluación: Cuando concluye una vuelta de al espiral, es momento de comenzar el ciclo de nuevo, pero no sin antes evaluar lo realizado en la iteración que termina. Esta evaluación, la realiza el jefe de desaroolo, de producción y el cliente.


## Toma de requerimientos
### Funcionales
- Login y Registro de los votantes y otros usuarios
- Seguridad en el login (2FA)
- Implementación de roles.
- Configuración de elección
- Información persistente
- Accesibilidad desde cualquier dispositivo

### No funcionales
- Diferentes ambientes (Producción y Desarrollo)
- Implementación de versionamiento en ramas y el proyecto
- Implementación de una arquitectura orientada a servicios
- Uso de contenedores
- Documentación del proyecto
- Implementar JWT
- Implementar 2FA
- Automatización utilizando pipelines
- Comunicación entre servicios
- Realización de pruebas unitarias.


## Historias de Usuario
- **Usuario**
- Como usuario quiero registrarme en el sistema
- Como usuario quiero ingresar mi voto

- **Administrador**
- Como administrador quiero registrarme en el sistema
- Como administrador quiero crear una elección
- Como administrador quiero editar una elección
- Como administrador quiero ver el estado de una elección
- Como administrador quiero realizar reportes por elección.


## Descripción de las tecnología a utilizar
- **Frontend**
    - **Angular**: Framkework utilizado para la vista resposive.
- **Backend**
    - **Nodejs**:Se utilizará para la creación de algunos servicios de aplicaciones API Rest
    - **Puthon**:Se utilizará para la creación de algunos servicios de aplicaciones API Rest
- **Contenerizar**
    - **Docker**: Se utilizará para la gestión de contenedores.
    - **_DockerHub**: Servicio para almacenar las imagenes de docker
- **Versionamiento**
    - **Gitlab**: Servicio para la gestión del repositorio y control de las versiones del proyecto.
- **Alojamiento**
    - **Google Cloud**: Utilizado para la creación de máquinas virtuales y la gestión de la base de datos
- **Seguridad**
    - **JWT**: Servicio para el uso de token en la autentificación.
. **Visualización del rendimiento**
    -**Prometheus:**: Sistema para monitorear el proyecto. 
    -**Grafana:**: Sistema para observar gráficamente los datos y la situación del proyecto / empresa.
- **Pruebas unitarias**
        - **Mocha**: Framework para pruebas en los servicios de Nodejs.
        - **Chai**: Librería para poder integrarla con Mocha y así tener un marco más legible y cómodo al momento de realizar las pruebas.
        - **Unittest**: Librería para pruebas en Python.


## Comunicación de los microservicios
![image1](Documentacion/image1.png)

## Diagrama de actividades del microservicio de autenticación
![image2](Documentacion/image2.png)

## Diagrama de actividades de emisión de voto

![image3](Documentacion/image3.png)

## Diagrama de actividades del sistema de registro de ciudadanos

![image4](Documentacion/image4.jpg)

## Descripción de la seguridad de la aplicación
Se implementará autentificación 2FA el cual nos ayudará a tener una mejor seguridad en la aplicación utilizando la app que nos ofrece Google authenticator y aplicándolo al sistema de yoVoto.<br>
También se implementará el uso del token JWT el cual se formará con el el id del usuario, el rol que lleva y su respectiva contraseña y asignándole una duración máxima de 1 hora.

## Descripción del uso de la tecnología blockchain

No poseo conocimientos para poder documentar esta parte :(